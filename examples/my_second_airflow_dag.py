
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.subdag_operator import SubDagOperator

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
}


dag = DAG(
    dag_id=DAG_NAME,
    default_args=args,
    schedule_interval="@once",
)



start = DummyOperator(
    task_id='start',
    default_args=args,
    dag=dag,
)


start = DummyOperator(
    task_id='start',
    default_args=args,
    dag=dag,
)



section_2 = SubDagOperator(
    task_id='section-2',
    subdag=subdag(DAG_NAME, 'section-2', args),
    default_args=args,
    dag=dag,
)


task_3 >> task_4


