# Airflow Generator

Generates from a directory structure an airflow DAG.

Note that this is a first experimental release. It is not finished yet. It just describes the idea.

## Motivation

Maintaining airflow DAGs should be dead simple.
Maintainers should not need deep knowledge about airflow internals.
They should focus more on tasks and its dependencies.
Usage of this fosters uniformity and simplicity in your DAGs.

## Folder Structure

```
examples/my_first_airflow_dag/
├── dependencies
├── my_first_task
└── my_second_task
```

`my_first_task` and `my_second_task` should be executables.
`dependencies` is a text file containing

```
my_first_task >> my_second_task
```

Based on this a DAG `my_first_airflow_dag` will be generated. It will have two tasks. `my_first_task` and `my_second_task`.
`my_first_task` will be executed first, then `my_second_task`.

## Support for sub-DAGs

Nesting these DAGs is possible by simply nesting the directories.

*Example:*

```
examples/my_second_airflow_dag/
├── dependencies
├── my_first_sub_dag
│   ├── dependencies
│   ├── task_1
│   └── task_2
├── task_3
└── task_4
```
