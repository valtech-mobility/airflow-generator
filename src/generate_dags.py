#!/usr/bin/env python3
from collections import namedtuple
import os

BODY = '''
import airflow
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.subdag_operator import SubDagOperator

args = {{
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
}}

{dag}

{tasks}

{subdags}

{dependencies}
'''

DAG = '''
dag = DAG(
    dag_id=DAG_NAME,
    default_args=args,
    schedule_interval="@once",
)
'''

TASK = '''
start = DummyOperator(
    task_id='start',
    default_args=args,
    dag=dag,
)
'''

SUBDAG = '''
section_2 = SubDagOperator(
    task_id='section-2',
    subdag=subdag(DAG_NAME, 'section-2', args),
    default_args=args,
    dag=dag,
)
'''

DAG_STRUCTURE = namedtuple(#
    'DAG_STRUCTURE',
    'name tasks subdags dependencies')

def scan_directory(path):
    tasks, subdags, dependencies = [], [], ''

    from glob import glob

    for item in glob(os.path.join(path, '*')):
        if os.path.basename(item) == 'dependencies':
            with open(item, 'r') as file:
                dependencies = file.read()
        if os.path.isdir(item):
            subdags.append(scan_directory(item))
        elif os.access(item, os.X_OK):
            tasks.append(os.path.basename(item))

    return DAG_STRUCTURE(
        name=os.path.basename(path),
        tasks=tasks,
        subdags=subdags,
        dependencies=dependencies,
    )

def render(dag):
    def render_dag(dag):
        return DAG.format()
    
    def render_tasks(tasks):
        return '\n'.join(TASK.format() for task in tasks)
    
    def render_subdags(subdags):
        return '\n'.join(SUBDAG.format() for subdag in subdags)

    return BODY.format(
        dag=render_dag(dag.name),
        tasks=render_tasks(dag.tasks),
        subdags=render_subdags(dag.subdags),
        dependencies=dag.dependencies,
    )

if __name__ == '__main__':
    import sys

    assert len(sys.argv) == 2
    path = sys.argv[-1]

    root = scan_directory(path)

    print(render(root))